---
layout: page
title: Strupler2013a-Radiocarbon
---

Code of the radiocarbon models published in: 

  - Strupler (2013a) [Neue Radiocarbon-Datierungen aus den Grabungen in der Unterstadt](https://www.academia.edu/7954663/Neue_Radiocarbon-Datierungen_aus_den_Grabungen_in_der_Unterstadt) *apud* A. Schachner, Die Arbeiten in Boğazköy-Hattusa 2012, Archäologischer Anzeiger 2013/1, 159-164 


## Radiocarbon samples
The radiocarbon samples [(data frame)](/Strupler2013a--Datation.csv) have been dated (AMS) by the *Leibniz-Labor für Altersbestimmung und Isotopenforschung* of the [Kiel University](http://www.uni-kiel.de/index-e.shtml).

Variable               |        Description
-----------------------|----------------------------------
  **Nr.**              | Row number of the data frame
  **Probe**            | Sample number from the Laboratory
  **Bodenprobe-Nr.**   | Sample number of the flotation
  **Fraktion**         | Material
  **(‰)**              | δ13C
  **Radiokarbonalter** | Date BP
  **Herkunft**         | Context
  **PQ**               | Trench

## Bayesian analysis
The radiocarbon determinations [(data frame)](/Strupler2013a--Datation.csv) are  statistically reconciled with the stratigraphy according to two models: 

-  [contiguous](Model-A--ContiguousModel.oxcal) 
-  [sequential](Model-B--SequentialModel.oxcal). 

The models have been generated using the [OxCal](https://c14.arch.ox.ac.uk/) v4.2.1* computer program [(Bronk Ramsey 2009)](http://c14.arch.ox.ac.uk/oxcalhelp/ref.html#bronkramsey2009bar) and using the 'INTCAL09' dataset [(Reimer 2009)](http://c14.arch.ox.ac.uk/oxcalhelp/ref.html#reimer2009imr). For details see [Strupler (2013a)](https://www.academia.edu/7954663/Neue_Radiocarbon-Datierungen_aus_den_Grabungen_in_der_Unterstadt). 

## Reuse potential
Build new models or import the models in the OxCal freeware and (re)run the calibrations. 


## Licences

  - Text: CC-BY (http://creativecommons.org/licenses/by/4.0/)

  - Code: MIT (http://opensource.org/licenses/MIT) year: 2014, copyright holder: Néhémie Strupler

  - Data: CC0 (https://creativecommons.org/publicdomain/zero/1.0/legalcode)

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title"></span> <a xmlns:cc="http://creativecommons.org/ns#" href="https://github.com/nehemie/Strupler2013a-Radiocarbon" property="cc:attributionName" rel="cc:attributionURL"> <a rel="license" href="http://creativecommons.org/licenses/by/4.0/"></a>