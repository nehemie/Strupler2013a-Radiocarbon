<!-- Mein herzlicher Dank gilt A. Schachner für die Überlassung von Materialien
aus den Grabungen Boğazköy. V. Egbers, \.{I}. Göçmez und S. Wittmann möchte ich für
die Zusammenarbeit bei der zeichnerischen Dokumentation und bei der
Datenbankeingabe ausdrücklich danken sowie J. Pröbstle für das Restaurieren der
Keramik.

Abbildungsnachweis: Abb. \ref{fig:UnmodeliertMBZ}--\ref{fig:IntervallDerMBZ}=
Boğazköy--Grabung, DAI (N. Strupler, erstellt mit OxCal (v4.2),
\cite{BronkRamsey2009}).  \vspace{0.2cm}

-->

Die Grabungen im Bereich nordwestlich von Kesikkaya (KNW)  in der Unterstadt
seit 2009 haben neue Materialien für Radiokarbonanalysen geliefert, die wiederum
Rückschlüsse auf die Entwicklung des Areals ermöglichen, und eine Grundlage für
historische Überlegungen schaffen[^1]. Einen Schwerpunkt bildet die ältere
Phase der Besiedlung der Unterstadt, die Mittelbronzezeit (MBZ), die durch eine Reihe
von Radiokarbondaten in diesem Areal nun erstmals eingegrenzt werden kann. Die
Einbindung den Radiokarbondatierungen in ihren Fundkontext und damit in ihre
Stratigraphie erlaubt eine wechselseitige Überprüfung der Datierungsansätze.

[^1]: Zum Einsatz von Radiokarbondaten in Boğazköy vgl. allgemein \cite{Schoop2006b}.


Die neuen Proben stammen aus drei verschiedenen Kontexten der ältesten
Bauschicht aus MBZ: dem ›Nord-Inventar‹,  dem ›Süd-Inventar‹ (Haus 82)[^2] und
einem Raum mit einem Ofen im Gebäude 91[^3] (Abb. 22). Der Stratigraphie des
Areals und der Keramik zufolge datieren die beiden Inventare in die MBZ
(_kārum_-Zeit), während das Gebäude 91 der hethitischen Periode zuzuschreiben
ist. Diese relative Reihenfolge ist gesichert und liegt der folgenden Analyse
als Prämisse zugrunde[^4]. 

[^2]: \cite[168--171]{Schachner2010}; \cite[36--41]{Schachner2011b};
  \cite[89--90]{Schachner2012b}; \cite{Strupler2011}.

[^3]: \cite[36 Abb. 9--10]{Schachner2011b}.

[^4]: Siehe Beitrag Schachner.


Das Material wurde durch Flotation aus Bodenproben gewonnen, von dem
Archäobotaniker Rainer Pasternark bestimmt[^5] und für die Untersuchung ausgewählt.
Die Analysen erfolgten im Leibniz-Labor für Altersbestimmung und
Isotopenforschung der Christian-Albrechts-Universität Kiel.  Die Proben, ihre
Herkunft, Fraktion und die Ergebnisse der Altersbestimmung durch AMS-Messungen
sind in einer Tabelle zusammengefasst (Tab. \ref{Tab-C14})[^6]. Die
Kalibrierung der Daten erfolgte mit der Software OxCal (v4.2)[^7] und dem
Datensatz IntCal09[^8]. 

[^5]: \cite{Pasternak2012}.

[^6]: Bei \cite[41]{Schachner2011b} und \cite[94]{Schachner2012}  wurden die Proben 7 und 8 schon publiziert.

[^7]: \cite{BronkRamsey2009}.

[^8]: \cite{Reimer2009}.



\afterpage{

\footnotesize
\begin{longtable}{cp{1cm}cp{1.2cm}cp{2.4cm}p{2.5cm}c}
\caption{Radiokarbonproben aus den Grabungen 2009–2010 am Kesikkaya-Nordwest} \label{Tab-C14} \\
    \toprule
Nr. & Probe & Bodenprobe-Nr. & Fraktion & $\delta^{13}C$ (\textperthousand) & Radiokarbonalter & Herkunft & PQ \\ 
    \midrule
1 &KIA 45939 & Bo10-177-325 &  verk. Samen %. Laugenrückstand. 4.1 mg C  %
& -23.79 $\pm$ 0.19 & 3855 +35/-30 BP & MBZ Nord-Inventar & 289/373 \\ 
2 & KIA 45938 & Bo10-85-1732 &  verk. Samen %. Laugenrückstand. 4.0 mg C %
 & -21.24 $\pm$ 0.21 & 3840 $\pm$ 25 BP & alth. Fußboden Gebäude 91 & 289/372 \\ 
3 &KIA 45940 & Bo10-112-1701 &  verk. Samen %. Laugenrückstand. 2.6 mg C %
& -24.21 $\pm$ 0.23 & 3655 $\pm$ 30 BP & MBZ Süd-Inventar (Gebäude 82) & 289/373 \\ 
4 &KIA 45945 & Bo10-112-497 &  verk. Samen %. Laugenrückstand. 1.3 mg C  %
& -25.04 $\pm$ 0.20 & 3615 $\pm$ 30 BP & MBZ Süd-Inventar (Gebäude 82) & 289/372 \\ 
5 &KIA 45941 & Bo10-177-362 &  verk. Samen %. Laugenrückstand. 3.8 mg C %
& -22.15 $\pm$ 0.29 & 3575 $\pm$ 30 BP  & MBZ Nord-Inventar & 289/373 \\ 
6 &KIA 45942 & Bo10-177-363 &  verk. Samen %. Laugenrückstand. 3.9 mg C  %
& -22.62 $\pm$ 0.15 & 3560 $\pm$ 30 BP & MBZ Nord-Inventar & 289/373 \\ 
7 &KIA 41842 & Bo09-178-470 &  Holzkohle %. Laugenrückstand. 2.4 mg C  %
& -22.05 $\pm$ 0.18 & 3515 $\pm$ 35 BP  & MBZ Nord-Inventar & 289/372 \\ 
8 &KIA 41843 & Bo09-207-1750 &  Holzkohle %. Laugenrückstand. 2.2 mg C   %
& -21.16 $\pm$ 0.22 & 3455 $\pm$ 20 BP & MBZ Süd-Inventar (Gebäude 82) & 289/372 \\ 
9 &KIA 45943 & Bo10-177-345 &  verk. Samen %. Laugenrückstand. 2.8 mg C %
& -23.12 $\pm$ 0.16 & 3410 $\pm$ 30 BP  & MBZ Nord-Inventar & 289/373 \\ 
10 &KlA 45944 & Bo10-85-1724 &  verk. Samen %. Laugenrückstand. 2.9 mg C  %
& -21.27 $\pm$ 0.11 & 3275 $\pm$ 40 BP  & alth. Fußboden Gebäude 91 & 289/372 \\ 
11 &KIA 45947 & Bo10-177-333 &  verk. Samen %. Laugenrückstand. 2.0 mg C %
& -22.73 $\pm$ 0.41 & 2765 $\pm$ 30 BP  & MBZ Nord-Inventar & 289/373 \\ 
    \bottomrule
\end{longtable}

\begin{figure}[h]
\centering
\includegraphics[width=0.8\textwidth]{./Imago/Abb23-1200dpi}
\caption{Kalibrierte Datierungen der mittelbronzezeitlichen Proben (3--9).}
\label{fig:UnmodeliertMBZ}
\end{figure}

\newpage


\begin{longtable}{rcccl}
  \caption{Datierung der ›Boundaries‹ nach dem Modell A (›Contiguous Model‹).} \label{Tab-BoudariesSequenceTransition} \\  
    \toprule
›Boundary‹ & Gewichteter Mittelwert & Median & 95 \%  ($\pm 2\sigma$)  & 68 \% ($\pm 1\sigma$)  \\ 
    \midrule
Anfang der MBZ Phase & 2047 BC & 2032 BC & 2195--1935 BC & 2076--1967 BC\\ 
Übergang MBZ / althetitischer Phase & 1687 BC & 1686 BC & 1852--1567 BC & 1742--1614 BC\\ 
Ende der althethitischen Phase & 1429 BC & 1486 BC & 1684--984 BC & 1621--1381 BC\\ 
    \bottomrule
\end{longtable}

\begin{figure}[h]
 \centering
  \includegraphics[width=0.8\textwidth]{./Imago/Abb24-1200dpi}
   \caption{Modell A (›Contiguous Model‹) der kalibrierten Radiokarbondatierungen 
   des Areals Kesikkaya Nordwest nach Bayes'scher Analyse, die auf 
   stratigraphischen Erkenntnissen basiert.}
 \label{fig:SequenceModellA}
\end{figure}

\newpage

\begin{longtable}{rcccl}
  \caption{Datierung der ›Boundaries‹ nach dem Modell B  (›Sequential Model\guilsinglleft).} \label{Tab-SequenceBoundaries}\\
      \toprule
  ›Boundary‹ & Gewichteter Mittelwert & Median & 95 \%  ($\pm 2\sigma$)  & 68 \% ($\pm 1\sigma$)  \\ 
      \midrule
  Anfang der MBZ Phase & 2019 BC & 2030 BC & 2154--1929 BC &  2058--1964 BC\\ 
  Ende der MBZ Phase & 1724 BC & 1732 BC & 1861--1620 BC & 1853--1656 BC\\ 
  Anfang der althethitischen Phase & 1617 BC & 1622 BC & 1776--1491 BC & 1681--1546 BC\\ 
  Ende der  althethitischen Phase & 1504 BC & 1463 BC & 1685--1106 BC & 1609--1430 BC\\ 
      \bottomrule
\end{longtable}

\begin{figure}[h!]
 \centering
  \includegraphics[width=\textwidth]{./Imago/Abb25-1200dpi}
  \caption{Modell B (›Sequential Model‹) der kalibrierten 
  Radiokarbondatierungen des Areals Kesikkaya Nordwest nach Bayes'scher Analyse. 
  Hier wurden zwei \textit{Boundaries} zwischen den beiden Phasen eingesetzt. Die 
  kalibrierten Datierungen wurden nicht dargestellt.}
 \label{fig:SequenceModellB}
\end{figure}
}

Von elf Proben werden drei nicht weiter in Betracht gezogen (Proben 1. 2. 11),
da ihre Daten nicht mit der Stratigraphie, der archäologischen Datierung der
Befunde und Funde sowie den Ergebnissen der Mehrheit der Proben vereinbar sind:
Von den Datierungen aus dem ›Nord-Inventar‹ (Probe 1. 5--7. 9. 11), dessen
Keramik der _kārum_-Zeit angehört,  ist die Probe 1 deutlich zu alt (3. Jt. v.
Chr.), die Probe 11 hingegen wesentlich zu jung (1. Jt. v. Chr.). Die Datierung
der Probe 2 in das 3. Jt. v. Chr.  ist ebenfalls zu verwerfen, weil das Gebäude
91 der Stratigraphie und der Keramik zufolge sicher in die hethitische Zeit
datiert. Im Gegensatz dazu kann die Probe 10 für die Datierung des Gebäudes 91
akzeptiert werden.


Bei der Kalibrierung markieren drei Farben die Herkunft der Proben: rot steht
für das ›Nord-Inventar‹, blau für das ›Süd-Inventar‹ und grün für die Probe aus
dem Gebäude 91. Die Radiokarbondatierungen aus der MBZ zeigen, daß es unmöglich
ist, die Inventare zu trennen: Die roten und blauen Datierungen sind miteinander
verwoben (Abb. \ref{fig:UnmodeliertMBZ}). Zwei der Proben der Inventare (Probe
7.8) stammen von Holzkohlefunden. Sie datieren nicht früher als die Proben aus
verkohlten Samen. Die Probe 8 ist sogar die jüngste des ›Süd-Inventars‹, das
Alter der Probe 7 liegt zwischen den aus den Samen des ›Nord-Inventars‹
gewonnenen Datierungen (Probe 5. 6. 9). Diese Verteilung der Datierungen deutet
darauf, daß sie nicht ein einziges Ereignis, sondern die Dauer einer Phase
dokumentieren.  Die Herkunft der Proben untermauert diese Interpretation: Im
Falle des ›Süd-Inventars‹ stammen die zwei ältesten Datierungen (Proben 3. 4.)
aus dem Lehmfußboden, während die jüngere (Probe 8) oberhalb desselben gefunden
wurde. Die Samen aus dem Fußboden (Probe 3. 4) sind im Laufe der Phase in den
Boden eingedrungen[^9].

[^9]: Um die Stratigraphie und Herkunft der Artefakte im Boden zu erklären, hat
  Michael Brian Schiffer diese Eigenschaft des Bodens _penetrability_ genannt.
\cite[126--129]{Schiffer1996}; \cite[21]{LaMotta1999}.

Die acht Proben der Mittelbronzezeit und der althethitischen Zeit wurden in zwei
Bayes'sche Modelle aufgenommen und anhand der Stratigraphie in zwei Phasen, eine
mittelbronzezeitliche und eine althethitische, unterteilt. Die Software `OxCal`
ermöglicht es, Sequenzierungsmodelle zu erstellen[^10], in denen die
kalibrierten Datierungen statistisch mit der Erkenntnissen aus der Stratigraphie
zusammengeführt und präzisiert werden[^11]. Um einer Phase eine Dauer
zuzuschreiben, muss diese von _Boundaries_ eingeschlossen werden[^12]. Bei der
Setzung der _Boundaries_ in einem Sequenzierungsmodell bieten sich zwei
Möglichkeiten: Entweder kann eine einzige _Boundary_ zwischen zwei Phasen
gesetzt werden, nach dem ›Contiguous Model‹ (Modell A), mit dem ein Datum für
den Übergang errechnet wird (Abb. \ref{fig:SequenceModellA}, Tab.
\ref{Tab-BoudariesSequenceTransition}). Oder es können zwei aufeinander folgende
_Boundaries_ zwischen zwei Phasen sethen, wenn man einen Hiatus vermutet, nach
dem sogenannten ›Sequential Model‹ (Modell B)[^13]. Dabei ermitteln die
_Boundaries_ den Anfang und das Ende beider Phasen, und ermöglichen die
Errechnung eines Intervalls zwischen denselben (Abb. \ref{fig:SequenceModellB}.
\ref{fig:IntervallDerMBZ}, Tab. \ref{Tab-SequenceBoundaries}). 

[^10]: In der Software `OxCal` wurden die Befehle _Sequence_, _Phase_,
  \textit{R\_Date}, _Boundary_ und _Interval_,  benutzt, um die Modelle zu
erstellen: Die Befehle werden kursiv und in eckigen Klammern in den Abbildungen
angegeben, der besseren Verständlichkeit wegen mit den englischen Begriffen.

[^11]: \cite[348--349]{BronkRamsey2009}.

[^12]: Boundaries sind ›Ereignisse‹, die vor und nach der Phase stattgefunden
  haben. Die Verwendung der _Boundaries_  setzt voraus, dass die
Radiokarbondatierungen innerhalb einer Phase ungeordnet sind; sie ermöglichen
es, Intervalle zu berechnen, \cite[345]{BronkRamsey2009}.

[^13]: \cite[348--349]{BronkRamsey2009}.



Im Modell A (›Contiguous Model‹) befindet sich die mittelbronzezeitliche Phase
innerhalb der Zeitspanne 2195--1567 v. Chr. (Sicherheitswahrscheinlichkeit von
95\%)  bzw. 2076--1614  v. Chr. (Sicherheitswahrscheinlichkeit von 60\%),  nach
dem Modell B (›Sequential Model‹) zwischen 2154--1620 v. Chr. bzw.  2058--1656
v. Chr.  Anhand der Mittelwerte und Mediane kann die mittelbronzezeitliche Phase
im Modell A etwa zwischen 2050 und 1685, bzw. im Modell B etwa zwischen 2030 und
1730 v. Chr. eingegrenzt werden (Tab. \ref{Tab-BoudariesSequenceTransition}.
\ref{Tab-SequenceBoundaries})[^14].

[^14]: Betont sei, daß es sich bei den Mittelwerten und Medianen
  (›Lageparameter‹) um in der Mitte einer bestimmten Skala liegende, statistisch
errechnete Werte handelt. Diese dienen lediglich dazu, die Werte
zusammenzufassen und eine zentrale Tendenz anzudeuten. Diese Werte können
ausdrücklich nicht als historisch genau angesehen werden. 


Obwohl das Material 2009--2010 aus zwei Inventaren  stammt, die vermutlich durch
denselben Brand zerstört wurden, zeigen die Datierungen eine große Zeitspanne.
Die Proben, die etwa in das 20. Jh. (Probe 3. 4), 19. Jh.  (Probe 5--7) bzw. 18.
Jh. v. Chr. (Probe 8. 9) datieren,  weisen auf eine lange Nutzungsdauer in der
mittelbronzezeitlichen Phase in diesem Bereich von Boğazköy hin. Dies  bedeutet,
daß zumindest dieses Gebäude während eines langen Zeitraums benutzt wurden, wie
die Proben 3 und 4 aus dem Lehmfußboden und die jüngere Probe zeigen. Die in den
Gebäuden gefundenen Artefakte  --  allen voran die Keramik -- sind hingegen
wegen ihrer kürzeren Lebensdauer in das Ende der Phase zu datieren[^15]. 


[^15]: \cite[57]{Strupler2011} zur vorläufigen Datierung der Keramik
der beiden mittelbronzezeitlichen Inventaren von Kesikkaya Nordwest.

Die durch die Radiokarbondatierungen aufgezeigte Zeitspanne der
mittelbronzezeitlichen Phase im Bereich Kesikaya-Nordwest deckt die beiden
Phasen der Unterstadt von Kültepe ab (›_kārum_-Kültepe‹ I und II), die dank
neuer philologischer Belege ca. 1970--1835 v. Chr.  bzw. 1830--1710 v. Chr.
datiert werden[^16]. Texte aus Kültepe und Boğazköy weisen die Existenz eines
_kārum_ in Boğazköy während der beiden Perioden  ›_kārum_-Kültepe‹ I und II[^17]
nach und passen damit zu den Ergebnissen der Radiokarbondatierungen.  

[^16]: \cite[28--40]{Barjamovic2012a}.

[^17]: \cite[294]{Barjamovic2011a}.


Die gewonnene Spanne der Datierungen läßt nicht unmittelbar auf historische
Ereignisse schließen. Trotz gewisser Übereinstimmungen kann eine Verbindung
zwischen den Zerstörungsereignissen der jeweiligen Befunde letztlich nicht
bewiesen, sondern nur als Hyphothese angenommen werden.  Wenn man eine
Zerstörung von Ḫattuš durch den König Anitta annimmt, wie sie im ›Anitta-Text‹
geschildert wird[^18], und und den Brand in dem in der südlichen Unterstadt
ausgegrabenen Gebäude mit diesem Ereignis in Verbindung bringt, stimmt das  das
aufgrund historischer Überlegungen vorgeschlagene Datum um ca.  1730 v. Chr.
jedoch auffällig mit den Radiokarbondatierungen, insbesondere der jüngsten
Datierung (Probe 9), überein[^19].

[^18]: CTH 1, \cite{Neu1974}.

[^19]: \cite[207]{Kryszat2008b}; \cite[39]{Barjamovic2012a}.

Um die Grenzen dieser Phase besser zu definieren, sind weitere Datierungen für
die althethitische Phase wünschenswert. Es liegt nur eine einzige Datierung aus
dem 16. Jh. v. Chr. vor (Probe 10, Abb. \ref{fig:Bo10-85-1724}), mit der kein gut
    strukturiertes Modell erstellen läßt. Dennoch kann so die Benutzung des
Gebäudes 91 im 16. Jh. v. Chr. nachgewiesen werden, d.h. in der gleichen Zeit,
in der gemäß der Untersuchung der Keramik auch das Gebäude 90 benutzt
wurde[^20]. Dies deutet auf die gleichzeitige Benutzung der Gebäude hin und
spricht damit für eine Nutzung des gesamten Areals in dieser Zeit[^21]. 

[^20]: Siehe Beitrag von Néhémie Strupler.

[^21]: Siehe Beitrag von Andreas Schachner.

Um der Frage nachzugehen, ob es die in dem ›Anitta-Text‹
geschilderte Zerstörung von Ḫattuš und darauffolgend einen Hiatus  am Ende der
MBZ in Boğazköy gab, ist es möglich, mit dem Modell B ein Intervall
zwischen den beiden Phasen am Kesikkaya-Nordwest zu errechnen (Abb.
\ref{fig:IntervallDerMBZ}). Demzufolge könnte zwischen ihnen mit der gleichen
statistischen Wahrscheinlichkeit  keine Zeit, oder aber ganze 141 Jahre
vergangen sein. Es muss also keinen Hiatus zwischen der mittelbronzezeitlichen
und der althethitischen Phase gegeben haben, dieser ist auch archäologisch bisher
nicht nachgewiesen; zumal über den mittelbronzezeitliche Befunden am
Kesikkaya-Nordwest keine Sedimentablagerung beobachtet wurde[^22].

[^22]: Zu philologischen Überlegungen über eine ›frühhethitische‹
Besiedlung von Boğazköy, s. \cite[25]{Beal2003}; \cite[51]{Barjamovic2012a}.


\begin{figure}[h]
 \centering
  \includegraphics[width=0.8\textwidth]{./Imago/Abb26-1200dpi}
  \caption{Kalibrierte Datierungen der Probe 10 (KLA 45944, Bo10-85-1724).}
 \label{fig:Bo10-85-1724}
\end{figure}

\begin{figure}[ht]
 \centering
  \includegraphics[width=0.8\textwidth]{./Imago/Abb27-1200dpi}
  \caption{Intervall zwischen der mittelbronzezeitlichen und der althetitischen 
  Periode nach dem Modell B (Abb. \ref{fig:SequenceModellB}).}
 \label{fig:IntervallDerMBZ}
\end{figure}

